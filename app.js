const express = require('express')
const app = express()
const cors = require('cors')
const apiRouter = require('./server/routes')
const errorHandler = require('./server/middlewares/errorHandler')
const PORT = process.env.PORT || 4000
const swaggerUI = require('swagger-ui-express')
const swaggerJsDoc = require('swagger-jsdoc')

// middlewares
app.use(cors())
app.use(express.urlencoded({extended: true}))
app.use(express.json())
app.use(errorHandler)

const options = {
  definition : {
    openapi: '3.0.0',
    info: {
      title: "Library API",
      version: "1.0.0",
      description: "Library API here"
    },
    servers: [
      {
        url: "http://localhost:4000"
      }
    ],
    
  },
  /**
   * Swagger documention on ./server/routes/v1/player.routes.js
   */
  apis: ["./server/routes/v1/*.js"]
}

const specs = swaggerJsDoc(options)
/**
 * @Routes /api
 * entrypoint for all API routes
 */

app.use("/api", apiRouter)
app.use("/api-docs", swaggerUI.serve,swaggerUI.setup(specs))


app.listen(PORT, () => {
  console.log(`Listening on http://localhost:${PORT}`)
})