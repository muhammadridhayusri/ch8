const PlayerController = require("../../controllers/player.controller");
const playerRouter = require("express").Router();

/**
 * @Routes "/api/v1/players"
 */

/**
 * @swagger
 * components:
 *  schemas:
 *    Players:
 *      type: object
 *      required:
 *      - username
 *      - email
 *      - password
 *      - experience
 *      - lvl 
 *      properties:
 *        id:
 *         type : string
 *         description: auto generated id players
 *        username:
 *          type: string
 *          description: username component
 *        password :
 *          type: string
 *          description: password component
 *        experience :
 *          type: integer
 *          description: experience from playing games
 *        lvl :
 *          type: integer
 *          description: u get from experience
 *           
 */

/**
 * @swagger
 * /api/v1/players/ :
 *  get:
 *      summary: Return the list of players
 *      tags: [Players]
 *      responses:
 *          200:
 *              description: list of players
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items: 
 *                              $ref: '#/components/schemas/Players'
 */

/**
 * @swagger
 * tags:
 *  name: Players
 *  description: the players managing
 */

/**
 * @swagger
 * /api/v1/players/ :
 *  post:
 *      summary: add the list of players
 *      tags: [Players]
 *      responses:
 *          201:
 *              description : add a list of players
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/Players'
 *          
 */

/**
 * @swagger
 * /api/v1/players/{id} :
 *  get:          
 *      summary: show players by id from params
 *      tags: [Players]
 *      parameters:
 *          -   in : path
 *              name: id
 *              schema: 
 *                  type: string
 *              required: true
 *              description: the players id
 *      responses:
 *          201:
 *              description : show players players by id
 *              content: 
 *                  application/json:
 *                      schema:
 *                              $ref: '#/components/schemas/Players'
 *          404: 
 *              description: The players was not found
 *          
 */
/**
 * @swagger
 * /api/v1/players/{id} :
 *  put:          
 *      summary: update players by id from params
 *      tags: [Players]
 *      parameters:
 *          -   in : path
 *              name: id
 *              schema: 
 *                  type: string
 *              required: true
 *              description: the players id
 *      responses:
 *          201:
 *              description : update players
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/Players'
 *          404:
 *              description: the players id not found    
 */

/**
 * @swagger
 * /api/v1/players/{id} :
 *  delete:
 *      summary : the id you want to delete
 *      tags: [Players]
 *      parameters: 
 *          -   in : path
 *              name: id
 *              schema:
 *                  type: string
 *              required: true
 *              description: the players id
 *      responses:
 *          201:
 *              description : delete players
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/Players'
 *          404:
 *              description: the players id not found  
 */

/**
 * @swagger
 * /api/v1/players/exp/{id} :
 *  post:
 *      summary : the id you want update experience
 *      tags: [Players]
 *      parameters: 
 *          -   in : path
 *              name: id
 *              schema:
 *                  type: string
 *              required: true
 *              description: the players id
 *      responses:
 *          201:
 *              description : update experience id
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/Players'
 *          404:
 *              description: the players id not found  
 */

playerRouter.get("/", PlayerController.getPlayers);
playerRouter.post("/", PlayerController.createPlayer);
playerRouter.get("/:id", PlayerController.getPlayerById);
playerRouter.put("/:id", PlayerController.updatePlayer);
playerRouter.delete("/:id", PlayerController.deletePlayer);
playerRouter.post("/exp/:id", PlayerController.updateExperience);

module.exports = playerRouter;
